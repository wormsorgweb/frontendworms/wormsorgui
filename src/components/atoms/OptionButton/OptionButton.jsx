import { Box, Typography, Button } from "@mui/material";

function OptionButton({ contentNumber, contentText, handleOnCLick, isResume }) {
  return (
    <Box>
      <Button
        sx={{ display: { xs: "none", sm: "block" } }}
        variant={isResume ? "navbarResumeButton" : ""}
        onClick={handleOnCLick}
      >
        <Typography variant="NavBarTextPrimary">
          {isResume ? "" : `${contentNumber}.`}{" "}
        </Typography>
        <Typography
          variant={isResume ? "NavBarTextPrimary" : "NavbarTextSecondary"}
        >
          {contentText}
        </Typography>
      </Button>
    </Box>
  );
}

export default OptionButton;
