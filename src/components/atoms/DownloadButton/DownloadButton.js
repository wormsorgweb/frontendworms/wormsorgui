import { Box, Typography, Button } from "@mui/material";

function DownloadButton({ contentText, documentName, documentFile }) {
  return (
    <Box>
      <a href={documentFile} download={documentName}>
        <Button variant="navbarResumeButton">
          <Typography variant={"NavBarTextPrimary"}>{contentText}</Typography>
        </Button>
      </a>
    </Box>
  );
}

export default DownloadButton;
