import { Grid } from "@mui/material";

import { CardProject } from "../index";
import ListProjects from "../../../data/data";

function ListCardsProject() {
  return (
    <Grid padding="0px 20px" container spacing={3}>
      {ListProjects.map((item) => {
        return (
          <Grid key={item.Id} item xs={12} sm={12} md={6} lg={6}>
            <CardProject project={item} />
          </Grid>
        );
      })}
    </Grid>
  );
}

export default ListCardsProject;
