import { Box, Typography, Link } from "@mui/material";
import styles from "./ProfileInformation.module.css";

function ProfileInformation() {
  return (
    <Box className={styles.container}>
      <Box className={styles.titleBody}>
        <Typography variant="BodyTextPrimary" component="span">
          ALEJANDRO ZEBALLOS
        </Typography>
      </Box>
      <Box className={styles.subtitleBody}>
        <Typography variant="BodyTextSecondary" component="span">
          Developing solutions since 2021!
        </Typography>
      </Box>
      <Box className={styles.textBody}>
        <Typography variant="BodySubTextSecondary" component="span">
          Hi! I’m software developer specializing in building solutions with
          .NET, React, NextJs and Docker passionate about software
          architecture.Positive attitude, empathic person, strong work ethic,
          driven by results person while focused on high code quality. Currently
          creating and supporting software at
        </Typography>
        <Typography variant="BodyLightTextSecondary" component="span">
          {" "}
          BCP
        </Typography>
      </Box>
      <Box className={styles.linkButton}>
        <Link
          href="https://gitlab.com/wormsorgweb/frontendworms/wormsorgui/-/tree/develop?ref_type=heads"
          rel="noopener"
          target={"_blank"}
          variant="buttonBody"
        >
          Check out the code of this side here
        </Link>
      </Box>
    </Box>
  );
}

export default ProfileInformation;
