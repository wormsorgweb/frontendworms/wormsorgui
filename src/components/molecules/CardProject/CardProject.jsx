import Image from "next/image";
import {
  Typography,
  Card,
  CardContent,
  CardHeader,
  Stack,
  Paper,
} from "@mui/material";
import styles from "./CardProject.module.css";

function CardProject({ project }) {
  return (
    <Card className={styles.container}>
      <CardHeader
        title={<Typography>{project?.TitleProject}</Typography>}
        className={styles.header}
      />
      <CardContent className={styles.cardContentContainer}>
        <Stack direction={{ xs: "column", sm: "column" }} spacing={2}>
          <Paper variant="CardPaperPrimary" elevation={0}>
            <Typography variant="ContentTextPrimary">
              {project?.Information}
            </Typography>
          </Paper>
          <Paper variant="CardPaperPrimary" elevation={0}>
            <Stack
              className={styles.stackUpContainer}
              direction="row"
              spacing={2}
            >
              {project?.ImagesUp.map((item) => {
                const componentImage = (
                  <Image
                    key={item.id}
                    src={item.ImageLink}
                    alt={item.ImageAlt}
                    width={item.Width}
                    height={item.Height}
                    className={styles.image}
                  />
                );
                return componentImage;
              })}
            </Stack>
            <Stack
              className={styles.stackDownContainer}
              direction="row"
              spacing={2}
            >
              {project?.ImagesDown.map((item) => {
                const componentImage = (
                  <Image
                    key={item.id}
                    src={item.ImageLink}
                    alt={item.ImageAlt}
                    width={item.Width}
                    height={item.Height}
                    className={styles.image}
                  />
                );
                return componentImage;
              })}
            </Stack>
          </Paper>
        </Stack>
      </CardContent>
    </Card>
  );
}

export default CardProject;
