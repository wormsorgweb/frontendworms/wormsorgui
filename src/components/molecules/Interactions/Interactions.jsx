import { Stack } from "@mui/material";
import { OptionButton, DownloadButton } from "../../atoms";
import useViewModel from "./ViewModel";

function Interactions() {
  const { options } = useViewModel();
  return (
    <Stack direction="row" spacing={1}>
      {options.map((option) =>
        option.isResume ? (
          <DownloadButton
            key={option.id}
            contentText={option.contentText}
            documentFile={option.documentFile}
            documentName={option.documentName}
          />
        ) : (
          <OptionButton
            key={option.id}
            contentNumber={option.contentNumber}
            contentText={option.contentText}
            handleOnCLick={option.handleOnCLick}
            isResume={option.isResume}
          />
        )
      )}
    </Stack>
  );
}

export default Interactions;
