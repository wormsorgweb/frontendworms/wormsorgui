import React from "react";
import { Box, Divider, Typography } from "@mui/material";

import { ListCardsProject } from "../../molecules";
import styles from "./ProjectsInformation.module.css";

export const ProjectsInformation = ({ isVisible }) => {
  return (
    <Box className={isVisible ? styles.containerShow : styles.containerHidden}>
      <Divider variant="BodyDivider" />
      <Typography variant="BodyTitlePrimary">Projects</Typography>
      <ListCardsProject />
    </Box>
  );
};
