import React from "react";
import { Box, AppBar, Toolbar, Typography } from "@mui/material";

import { Interactions } from "../../molecules";
import styles from "./NavBar.module.css";

export const NavBar = () => {
  return (
    <Box className={styles.containerNavBar}>
      <AppBar
        className={styles.containerAppBar}
        position="static"
        color="inherit"
      >
        <Toolbar variant="navBar">
          <Box className={styles.container}>
            <Typography variant="navbarResumeButton">AZ</Typography>
          </Box>
          <Interactions />
        </Toolbar>
      </AppBar>
    </Box>
  );
};
