import React, { useState, useEffect } from "react";
import { Box, Slide } from "@mui/material";

import { PrincipalContent, ProjectsInformation } from "../../organisms";

export default function Principal() {
  const [isSectionVisible, setIsSectionVisible] = useState(false);

  useEffect(() => {
    let computeProgress = () => {
      const scrolled = document.documentElement.scrollTop;
      const scrollLength =
        document.documentElement.scrollHeight -
        document.documentElement.clientHeight;
      const progress = `${(100 * scrolled) / scrollLength}`;
      if (progress > 5) {
        setIsSectionVisible(true);
      } else {
        setIsSectionVisible(false);
      }
    };
    window.addEventListener("scroll", computeProgress);
    return () => window.removeEventListener("scroll", computeProgress);
  });

  return (
    <Box>
      <PrincipalContent />
      <Slide direction="up" in={isSectionVisible}>
        <Box>
          <ProjectsInformation isVisible={isSectionVisible} />
        </Box>
      </Slide>
    </Box>
  );
}
