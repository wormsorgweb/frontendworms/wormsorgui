export default function Divider(theme) {
  return {
    MuiDivider: {
      variants: [
        {
          props: { variant: "BodyDivider" },
          style: {
            borderBottomWidth: "2.6px",
          },
        },
      ],
    },
  };
}
