import { merge } from "lodash";

import Typography from "./TypographyVariant";
import Button from "./Button";
import ToolBar from "./ToolBar";
import Link from "./Link";
import Paper from "./Paper";
import Divider from "./Divider";

export default function ComponentsOverrides(theme) {
  return merge(
    Typography(theme),
    Button(theme),
    ToolBar(theme),
    Link(theme),
    Paper(theme),
    Divider(theme)
  );
}
