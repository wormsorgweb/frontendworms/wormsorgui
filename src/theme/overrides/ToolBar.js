export default function ToolBar(theme) {
  return {
    MuiToolbar: {
      variants: [
        {
          props: { variant: "navBar" },
          style: {
            color: "inherit",
            backgroundColor: "inherit",
            justifyContent: "space-between",
            height: "100%"
          },
        },
      ],
    },
  };
}
