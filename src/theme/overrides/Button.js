export default function Button(theme) {
  return {
    MuiButton: {
      variants: [
        {
          props: { variant: "navbarResumeButton" },
          style: {
            border: "solid ",
            borderColor: theme.palette.text.primary,
            paddingRight: "5px",
            paddingLeft: "5px",
          },
        },
      ],
    },
  };
}
