export default function Typography(theme) {
  return {
    MuiTypography: {
      variants: [
        {
          props: { variant: "NavBarTextPrimary" },
          style: {
            ...theme.typography.subtitle1,
            color: theme.palette.text.primary,
            marginRight: "5px",
          },
        },
        {
          props: { variant: "NavbarTextSecondary" },
          style: {
            ...theme.typography.subtitle1,
            color: theme.palette.text.secondary,
          },
        },
        {
          props: { variant: "BodyTextPrimary" },
          style: {
            ...theme.typography.h1,
            color: theme.palette.text.primary,
          },
        },
        {
          props: { variant: "BodyTextSecondary" },
          style: {
            ...theme.typography.h2,
            color: theme.palette.text.secondary,
          },
        },
        {
          props: { variant: "BodySubTextSecondary" },
          style: {
            ...theme.typography.h6,
            color: theme.palette.text.secondary,
          },
        },
        {
          props: { variant: "BodyLightTextSecondary" },
          style: {
            ...theme.typography.h6,
            color: theme.palette.text.primary,
          },
        },
        {
          props: { variant: "ContentTextPrimary" },
          style: {
            ...theme.typography.body1,
            color: theme.palette.text.secondary,
          },
        },
        {
          props: { variant: "BodyTitlePrimary" },
          style: {
            ...theme.typography.h1,
            color: theme.palette.text.primary,
            
          },
        },
      ],
    },
  };
}
