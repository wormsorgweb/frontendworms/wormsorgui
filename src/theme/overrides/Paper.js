export default function Paper(theme) {
  return {
    MuiPaper: {
      variants: [
        {
          props: { variant: "CardPaperPrimary" },
          style: {
            width: "100%",
            [theme.breakpoints.down('sm')]: {
              width: "100%",
            },
          },
        },
      ],
    },
  };
}
