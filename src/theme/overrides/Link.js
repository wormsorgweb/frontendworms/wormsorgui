export default function Link(theme) {
  return {
    MuiLink: {
      variants: [
        {
          props: { variant: "buttonBody" },
          style: {
            ...theme.typography.body1,
            color: theme.palette.text.primary,
            border: "2px solid",
            borderColor: theme.palette.text.primary,
            borderRadius: "10px",
            textDecoration: "none",
            padding: "10px 30px",
          },
        },
      ],
    },
  };
}
