import React from "react";

import { NavBar } from "../components/organisms";
export default function Layout({ children }) {
  return (
    <>
      <NavBar />
      <main>{children}</main>
    </>
  );
}
