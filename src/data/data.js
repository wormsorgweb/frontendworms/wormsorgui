const data = [
  {
    Id: "fdfba8bc-8053-4ca5-b685-e87b3287a59b",
    TitleProject: "Rekognition",
    Information:
      "Created an AI-based application to validate identification cards: \n Added a new feature to validate identification cards using Blazor. \n  Included configuration to manage a second camera on mobile devices. \n Implemented a filter to identify the identification card.",
    ImagesUp: [
      {
        id: 1,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685241315/.net_mcic9g.png",
        ImageAlt: ".Net Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 2,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1693162326/BrandBlazor_nohalo_1000x_tvl0qj.png",
        ImageAlt: "Blazor Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 3,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323469/vertical-logo-monochromatic_oypdj8.webp",
        ImageAlt: "Docker Image",
        Width: 100,
        Height: 100,
      },
    ],
    ImagesDown: [
      {
        id: 4,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323947/azure_1_xdsgak.png",
        ImageAlt: "React Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 5,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685290830/microsoft-iis2928_m5tl8u.jpg",
        ImageAlt: "ISS Image",
        Width: 100,
        Height: 100,
      },
    ],
  },
  {
    Id: "d6f58fd5-d47a-42b8-9ba4-8d650dcd729a",
    TitleProject: "Plan Accounts Manager",
    Information:
      "Created an application to manage plans between different accounts: \n - Implemented functionality to integrate with other systems for retrieving and updating property plans. \n - Developed new views using ASP.NET with Bootstrap and jQuery. \n - Migrated the core from WCF 3 to .NET 6 with ASP.NET and REST API.",
    ImagesUp: [
      {
        id: 1,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685241315/.net_mcic9g.png",
        ImageAlt: ".Net Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 2,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323947/azure_1_xdsgak.png",
        ImageAlt: "Python Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 3,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1693186516/asp-net_zvpzsh.webp",
        ImageAlt: "Amazon Lambda Image",
        Width: 100,
        Height: 100,
      },
    ],
    ImagesDown: [
      {
        id: 4,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1693186657/blogaz/sql-server-tutorial_awrtkd.svg",
        ImageAlt: "React Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 5,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685290830/microsoft-iis2928_m5tl8u.jpg",
        ImageAlt: "ISS Image",
        Width: 100,
        Height: 100,
      },
    ],
  },
  {
    Id: "c566b624-da92-4c3f-a28c-8857ada4fbf0",
    TitleProject: "DIARA",
    Information:
      "Created an application based on IA: \n - Created application for chat with a pdf with React js, reduxtool-kit, axios and material ui and implementing concepts of development like atomic design with reusable components, mvvm architecture and architecture as a code. \n  - Managed small team using scrum with azure boards. \n - Developed a chat implementing OpenIA API using .net, AWS Lambda functions with python and strategies of promting.",
    ImagesUp: [
      {
        id: 1,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685241315/.net_mcic9g.png",
        ImageAlt: ".Net Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 2,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685241329/1_mwfqygyvyqbzkkbphs5j_g_vcvyrt.jpg",
        ImageAlt: "Python Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 3,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685241334/1_RZQqQFLWJLJtPX5YZLGooQ_wdkq43.png",
        ImageAlt: "Amazon Lambda Image",
        Width: 100,
        Height: 100,
      },
    ],
    ImagesDown: [
      {
        id: 4,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685241346/react-1_zjjr6l.svg",
        ImageAlt: "React Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 5,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685290830/microsoft-iis2928_m5tl8u.jpg",
        ImageAlt: "ISS Image",
        Width: 100,
        Height: 100,
      },
    ],
  },
  {
    Id: "3d8c2a0c-8bab-4fc1-8917-a39b31bc7c85",
    TitleProject: "ARMELIN",
    Information:
      "Created an application with react and .net: - Created application for manage and create user information and accounts with React js, reduxtool-kit, axios and material ui. \n - Created an Api with C# and .net 6 using n-tier layer . \n - Created an gateway with Ocelot for comunication between frontend and backend \n - Created CI/CD flows with yml using azure devops. \n - Implement image recognition with docker and .net",
    ImagesUp: [
      {
        id: 6,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323947/azure_1_xdsgak.png",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 7,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685241346/react-1_zjjr6l.svg",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 8,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685291372/3knjcifnxywlyyxrvdyj_xipdhd.webp",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
    ],
    ImagesDown: [
      {
        id: 9,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685241315/.net_mcic9g.png",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 10,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685290830/microsoft-iis2928_m5tl8u.jpg",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
    ],
  },
  {
    Id: "55aec409-2c61-4bb9-a269-a50d1e7cc9d3",
    TitleProject: "MEETPOINT",
    Information:
      "Overlord a web application designed for communication between participants (students most of the time) through chats and videocalls and the user could see information about courses on the map with geojson and could create conversations between participants on a course. \n - Designed components (atoms, molecules and organism) with material ui for React js applying atomic design and architecture as a code using diagram components with plantUml. \n - Add initial theme provider configuration with Material ui and react js. \n - Implemented initial configuration for custom hooks and constants. \n - Created custom hook for transform dates and hours to user localtime with javascript. \n - Created reusable components with react js. \n - Created new slices for manage state of components and retrieve data from backend with redux-tool-kit and axios.  \n - Created extensions methods for pagination and sorting with mongo db driver and .net. \n - Updated sql tables and documents on db with Liquibase using sql(mysql) and metadata for no relation db (mongo). \n - Add unit tests using xunit and moq with .Net Core. \n - Improve update record processes performance from 17 seg to 0.02 seg creating bulk updates endpoints with entity framework, mongo db driver, cosmosdb and .net. \n - Researched and implemented feature flags using flagsmith and react. \n - Researched about how to implemented geojson ubication with leaflet and react.",
    ImagesUp: [
      {
        id: 12,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685241346/react-1_zjjr6l.svg",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 13,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685241315/.net_mcic9g.png",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 14,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323059/GFz_P-5e_400x400_e9iugf.png",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
    ],
    ImagesDown: [
      {
        id: 15,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685322935/open-graph-gitlab_hhwcsi.png",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 16,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323067/logo_odgdcb.svg",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
    ],
  },
  {
    Id: "f514e5bd-81f6-4b50-a8c2-2914534c6e3c",
    TitleProject: "APPLICANT TRACKER",
    Information:
      "Applicant tracker a web application designed to manage information for recruiters, The user could add, import, export and view information about applicants, modules and activities. - Created mongodb and sqlserver services using scripts for up all the tables, collections and initial data with docker-compose. \n - Created initial config applicants microservice using n-tier layer architecture with .Net Core.  \n - Created redux, sagas and axios config for consume applicant and activities microservice through kong Gateway and map to redux state with React. \n - Created reusable widgets for show relevant applicants information taking care of the composition with material UI, CSS and React. \n - Created filter activities endpoint using mongodbDriver with .Net Core. \n - Created middleware for customize bad request and internal server error responses with .Net Core. \n - Updated storage procedures with new requirements using Dapper with sql server and .Net Core. \n - Add unit tests using xunit and moq with .Net Core. \n - Updated RabbitMQ configuration with new queue, consumer and producer for communicate two microservices. \n - Created notifications microservices for send emails using rabbitmq and sendgrid with .Net Core. \n - Researched about how to implemented cloudinary microservice for manage images and documents with .Net Core. \n - Created and updated documentation of more than ten microservices based on model c4 until level 3 with draw.io.",
    ImagesUp: [
      {
        id: 8,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685241346/react-1_zjjr6l.svg",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 9,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685241315/.net_mcic9g.png",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 10,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323947/azure_1_xdsgak.png",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
    ],
    ImagesDown: [
      {
        id: 11,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323260/kong-api-gateway_obixa4.webp",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 12,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323265/rabbitmq-icon-484x512-s9lfaapn_urbuev.png",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
    ],
  },
  {
    Id: "8bc93d71-a9fb-4914-95d1-01eb09f9da6b",
    TitleProject: "MUNAY",
    Information:
      "Web application for manage small companies \n - Deployed application in a linux server on cloudinary using docker and docker compose. \n - Created microservice for send emails with express and sendgrid.",
    ImagesUp: [
      {
        id: 17,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323477/041cYVio_400x400_qhmofq.jpg",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 18,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323493/nodejslogo_y4pzq4.png",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 19,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323481/62e14e03eb4d9a9dc054c18e_xipukb.png",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
    ],
    ImagesDown: [
      {
        id: 20,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323469/vertical-logo-monochromatic_oypdj8.webp",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 21,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323620/GitHub_Invertocat_Logo_jy41ej.svg",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
    ],
  },
  {
    Id: "2bc2b895-e0fe-454c-bc94-2b7b2cad3842",
    TitleProject: "Assistant AEE",
    Information:
      "Assistant AEE a Software designed for detect stress, anxiety and depression through a test and connect users with therapists using Codeigniter and Bootstrap. \n - Users could fill tests and watch The results. \n - Designed the flow and screens for a normal user and therapist with Figma. \n - Created a monolithic application with Users CRUD, therapist CRUD and tests CRUD with Codeigniter. \n - Created integration with email notifications for users and therapist with Codeigniter email library. \n  - Created integration with google charts for show test results in a period of time. \n - Create a panic button email notification in case a user needs help from a therapist. \n - Config production environment on 000webhost.",
    ImagesUp: [
      {
        id: 22,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323620/GitHub_Invertocat_Logo_jy41ej.svg",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 23,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323832/PHP_a2zyqw.webp",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 24,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323839/codeigniter-tutorial_sbxngn.png",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
    ],
    ImagesDown: [
      {
        id: 25,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323839/Database-mysql.svg_dy3rpl.png",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
      {
        id: 26,
        ImageLink:
          "https://res.cloudinary.com/dwnxvydkv/image/upload/v1685323846/Figma-logo_innzuw.svg",
        ImageAlt: "Tech Image",
        Width: 100,
        Height: 100,
      },
    ],
  },
];

export default data;
