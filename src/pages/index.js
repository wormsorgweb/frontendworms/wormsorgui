import Head from "next/head";
import { Principal } from "../components/Containers";

export default function Home() {
  return (
    <>
      <Head>
        <title>Portfolio Alejandro Zeballos</title>
        <meta name="description" content="Portfolio Alejandro Zeballos" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/images/notFound.png" />
      </Head>
      <main>
        <Principal />
      </main>
    </>
  );
}
