import { CssBaseline } from "@mui/material";
import ThemeConfig from "../theme/ThemeMUI";
import Layout from "../Layouts/Layout";
import "@/styles/globals.css";

export default function App({ Component, pageProps }) {
  return (
    <ThemeConfig>
      <CssBaseline />
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ThemeConfig>
  );
}
