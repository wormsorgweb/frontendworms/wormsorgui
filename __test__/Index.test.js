import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Home from "../src/pages/index";

describe("Home", () => {
  it("renders an example", () => {
    render(<Home />);

    const heading = screen.getByText(/ALEJANDRO ZEBALLOS/i);

    expect(heading).toBeInTheDocument();
  });
});
